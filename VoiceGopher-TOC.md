Terms and Conditions

 

Terms and Conditions ("Terms")


Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the VoiceGopher skill (the "Skill") operated by Enke Systems, Inc ("us", "we", or "our").
Your access to and use of the Skill is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Skill.
By accessing or using the Skill you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Skill.


Accounts

We don't store any passwords on our service and the entire Authentication is done via Amazon Oauth service.
When you create an account with us, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Skill.
You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Skill or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.


Links To Other Web Sites

Our Skill may contain links to third party web sites or services that are not owned or controlled by Enke Systems, Inc.
Enke Systems, Inc has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.
You acknowledge and agree that Enke Systems, Inc shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.
We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.

Termination

We may terminate or suspend your account and bar access to the Skill immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.
If you wish to terminate your account, you may simply discontinue using the Skill.
All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.

Indemnification

You agree to defend, indemnify and hold harmless Enke Systems, Inc and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Skill, by you or any person using your account and password, or b) a breach of these Terms.

Limitation Of Liability

In no event shall Enke Systems, Inc, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Skill; (ii) any conduct or content of any third party on the Skill; (iii) any content obtained from the Skill; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.

Disclaimer

Your use of the Skill is at your sole risk. The Skill is provided on an "AS IS" and "AS AVAILABLE" basis. The Skill is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.
Enke Systems, Inc its subsidiaries, affiliates, and its licensors do not warrant that a) the Skill will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Skill is free of viruses or other harmful components; or d) the results of using the Skill will meet your requirements.

Exclusions

Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.

Governing Law

These Terms shall be governed and construed in accordance with the laws of Delaware, United States, without regard to its conflict of law provisions.
Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Skill, and supersede and replace any prior agreements we might have had between us regarding the Skill.

Changes

We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 15 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
By continuing to access or use our Skill after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Skill.

Contact Us

If you have any questions about these Terms, please contact us (nkanayan@gmail.com).